<?php 
namespace JGOULET\TestBundle\Services;

// Sercvice lengow_test
class LengowTest {

    // Fonction permettant de récupérer le contenu du fichier xml
    public function getXml($url) {

        // On convertit le fichier XML en Objet
        $xml = simplexml_load_file($url);

        // Permet d'afficher le contenu d'une variable pour s'assurer que l'objet n'est pas vide
        print_r($xml);
        
    }
}