<?php

namespace JGOULET\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('JGOULETTestBundle:Default:index.html.twig', array('name' => $name));
    }

    public function fluxAction()
    {
        // On récupère le paramètre url_orders du fichier con
        $url = $this->container->getParameter('url_orders');
        // On retourne le résultat du service lengow_test
        return new Response($this->get('jgoulet_test.lengow_test')->getXml($url));
        
        
        //$return=json_encode(...);
        //return new Response($return,200,array('Content-Type'=>'application/json'));
    }
}
