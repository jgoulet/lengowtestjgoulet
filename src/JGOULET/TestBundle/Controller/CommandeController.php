<?php
// src/JGOULET/TestBundle/Controller/CommandeController.php

namespace JGOULET\TestBundle\Controller;

use JGOULET\TestBundle\Entity\Commande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommandeController extends Controller
{
  public function addAction(Request $request)
  {
    // Création de l'entité
    $order = new Commande();
    $order->setMarketplace();
    $order->setOrderPurchaseDate();
    $order->setOrderAmount();
    $order->setOrderShipping();

    // On récupère l'EntityManager
    $em = $this->getDoctrine()->getManager();

    // On persiste l'entité (celle-ci est gérée par Doctrine)
    $em->persist($order);

    // On "flush" tout ce qui a été persisté avant (éxécution des requêtes afin de sauvegarder les données)
    $em->flush();

    // Reste de la méthode qu'on avait déjà écrit
    if ($request->isMethod('POST')) {
      $request->getSession()->getFlashBag()->add('notice', 'Commande bien enregistrée.');
      return $this->redirect($this->generateUrl('jgoulet_test_view', array('id' => $order->getId())));
    }

    return $this->render('JGOULETTestBundle:Commande:add.html.twig');
  }
}